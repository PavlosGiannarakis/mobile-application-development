package pavlosgiannarakis40110403.coursework;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class DataProtection {
    private Context context;
    private DatabaseManipulator dm;

    public DataProtection(Context context)
    {
        this.context=context;
    }

    public int DataValidation(String name, String surname, String id, String email,
                                  String role, String password, String password2)
    {
        int i = 0;
        String x = "";
        if (name.trim().length() >=2 && isNotNumeric(name) == true)
        {
            i++;

        }

        if (surname.trim().length() >=2 && isNotNumeric(surname) == true)
        {
            i++;
        }

        if (role.trim().equals("Student"))
        {
            if (id.trim().length() == 8 && IdExists(id,role)==true)
            {
                i++;
            }
        }
        else if (role.trim().equals("Teacher"))
        {
            if (id.trim().length() <= 8 && IdExists(id,role))
            {
                i++;
            }
        }
        else if ( role.trim().equals("Admin"))
        {
            i++;
        }

        if(validEmail(email)==true)
        {
            i++;
        }

        if (validPassword(password,password2)==true)
        {
            i++;

        }

        if (i==5)
        {
            return i;
        }
        else
        {
            return i;
        }

    }

    public boolean IdExists(String id, String role){
        dm = new DatabaseManipulator(context);
        if (dm.valid_id(id,role) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean AdminDataValidation(String id, String role) {
        int i=0;
        if (role.trim().equals("Student"))
        {
            if (id.trim().length() == 8 && isNumeric(id) == true)
            {
                i++;
            }
        }
        else if (role.trim().equals("Teacher"))
        {
            if (id.trim().length() <= 8)
            {
                i++;
            }
        }

        if(i == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean validEmail(String email)
    {
        if (email.contains("@")==true && email.contains("@.") == false && email.contains(".") == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean validPassword(String pass, String pass2)
    {
        if (pass.equals(pass2) == true && pass.trim().length() >= 6 && good_format(pass)==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException p)
        {
            return false;
        }
        return true;
    }

    public boolean isNotNumeric(String s) {
        if(s.matches("[a-zA-Z ]+") == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean good_format(String rdm) {
        char[] chars = rdm.toCharArray();
        int i=0,j=0,x=0,y=0;

        for (char c : chars) {
            if(Character.isLetter(c)) {
                i++;
                if(Character.isUpperCase(c)){
                    x++;
                }
                else if(Character.isLowerCase(c))
                {
                    y++;
                }
            }
            else if(Character.isDigit(c)){
                j++;
            }
        }
        if (i>=1&& x>=1&& y>=1 && j>=1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean valid_date(String date,String date_new)
    {
        String[] dates = date.split("-");
        int day = Integer.parseInt(dates[0]);
        int month = Integer.parseInt(dates[1]);
        int year = Integer.parseInt(dates[2]);
        String[] dates2 = date_new.split("-");
        int day1 = Integer.parseInt(dates2[0]);
        int month1 = Integer.parseInt(dates2[1]);
        int year1 = Integer.parseInt(dates2[2]);

        if (year1>year)
        {
            return true;
        }
        else if (year1>=year && month1>month)
        {
            return true;
        }
        else if (year1>=year && month1>=month && day1>day)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int possiblecap(String cap_old, String cap_new)
    {
        int x = Integer.parseInt(cap_old);
        int y = Integer.parseInt(cap_new);
        int result = x - y;
        return result;
    }
}
