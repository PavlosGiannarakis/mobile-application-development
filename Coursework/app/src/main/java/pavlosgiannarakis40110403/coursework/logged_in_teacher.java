package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class logged_in_teacher extends Activity implements View.OnClickListener {
    private DatabaseManipulator dm;
    private DataProtection dp;

    private EditText DateEventet;
    private EditText DateEventet2;

    private DatePickerDialog onDatePickerDialog;
    private DatePickerDialog onDatePickerDialog2;

    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logged_in_teacher);
        dm = new DatabaseManipulator(this);
        dp = new DataProtection(this);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);

        DateEventet = (EditText) findViewById(R.id.et_date_on);
        DateEventet.setInputType(InputType.TYPE_NULL);
        DateEventet.requestFocus();

        DateEventet2 = (EditText) findViewById(R.id.et_date_modify);
        DateEventet2.setInputType(InputType.TYPE_NULL);

        setDateTimeField();

        Bundle extras = getIntent().getExtras();
        String value = extras.getString("User");
        Log.i("test", value);

        final TextView textViewToChange = (TextView) findViewById(R.id.user_wlc);
        textViewToChange.setText("Welcome "+dm.getName(value));
        loadSpinnerData();

    }

    private void loadSpinnerData()
    {
        Spinner campus = (Spinner) findViewById(R.id.campus_at_sp);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.campus_select, android.R.layout.simple_spinner_item);
        campus.setAdapter(adapter);

        Spinner host = (Spinner) findViewById(R.id.hosted_by_sp);
        List<String> labels = dm.getTeachers();
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, labels);
        host.setAdapter(adapter2);

        Spinner campus_up = (Spinner) findViewById(R.id.hosted_at_modify_sp);
        campus_up.setAdapter(adapter);

        Spinner host_up = (Spinner) findViewById(R.id.hosted_by_modify_sp);
        host_up.setAdapter(adapter2);

        Spinner event_canceller = (Spinner) findViewById(R.id.to_be_cancelled);
        List<String> events = dm.getEvents();
        ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, events);
        event_canceller.setAdapter(adapter5);
        Spinner events_available = (Spinner) findViewById(R.id.events_available_sp);
        events_available.setAdapter(adapter5);
        //watch
    }

    private void setDateTimeField() {
        DateEventet.setOnClickListener(this);
        DateEventet2.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        onDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEventet.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        onDatePickerDialog2 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                DateEventet2.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public void onCancel(View v)
    {
        Spinner event_canceller = (Spinner) findViewById(R.id.to_be_cancelled);
        String event = event_canceller.getSelectedItem().toString().trim();
        String[] parts = event.split(" - ");
        String event_id = parts[0].trim();
        String event_name = parts[1].trim();
        String reason = ((EditText)findViewById(R.id.et_reason_cancel)).getText().toString().trim();
        dm.insert5(event_id, event_name, reason);
        dm.cancelEvent(event_id);
        reset2();
        Toast.makeText(getBaseContext(),"Event cancelled",Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v)
    {
        Spinner hb = (Spinner) findViewById(R.id.hosted_by_sp);
        Spinner ca = (Spinner) findViewById(R.id.campus_at_sp);
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("User");

        Random x = new Random();
        int id =x.nextInt(100000 - 10000) + 10000;

        String event_id = Integer.toString(id);
        String event_name=((EditText)findViewById(R.id.et_event_name)).getText().toString().trim();
        String created_on = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        String host =hb.getSelectedItem().toString();
        String campus_at = ca.getSelectedItem().toString();
        String created_by = value.trim();
        String status = "Scheduled";
        String date= ((EditText)findViewById(R.id.et_date_on)).getText().toString().trim();
        String capacity=((EditText)findViewById(R.id.et_capacity)).getText().toString().trim();

        int i = 0;
        switch (v.getId()) {

            case R.id.et_date_on:
                onDatePickerDialog.show();
                break;
            case R.id.et_date_modify:
                onDatePickerDialog2.show();
                break;
            case R.id.sbmt_c_ev:
                if (dp.isNotNumeric(event_name)==true && dp.valid_date(created_on,date) == true
                        && dp.isNumeric(capacity)==true) {
                    dm.insert3(event_id, event_name, date, campus_at, host, created_on, created_by, status,
                            capacity);
                    dm.insert4(event_id,capacity,capacity);
                    Toast.makeText(getBaseContext(), "Event created", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getBaseContext(), "Please correct Information ", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void onTvClick(View v) {
        Log.i("So it ", "works");
        Intent show_students = new Intent(logged_in_teacher.this,Show_list.class);
        LinearLayout create = (LinearLayout) findViewById(R.id.create_event_ll);
        LinearLayout update = (LinearLayout) findViewById(R.id.update_event_ll);
        LinearLayout cancel = (LinearLayout) findViewById(R.id.cancel_event_ll);
        LinearLayout select = (LinearLayout) findViewById(R.id.select_to_update);
        LinearLayout after_sel = (LinearLayout) findViewById(R.id.parent_ll_update);

        switch (v.getId()) {
            case R.id.show_stu:
                show_students.putExtra("Selected", "Student");
                show_students.putExtra("Selection", "");
                startActivity(show_students);
                break;
            case R.id.show_event:
                show_students.putExtra("Selected", "Event");
                show_students.putExtra("Selection", "");
                startActivity(show_students);
                break;
            case R.id.create_event:
                if (create.getVisibility() == View.VISIBLE) {
                    create.setVisibility(View.GONE);
                    break;
                } else if (create.getVisibility() == View.GONE) {
                    create.setVisibility(View.VISIBLE);
                    break;
                }
            case R.id.update_event:
                loadSpinnerData();
                if (update.getVisibility() == View.VISIBLE) {
                    update.setVisibility(View.GONE);
                    select.setVisibility(View.GONE);
                    after_sel.setVisibility(View.GONE);
                    break;
                } else if (update.getVisibility() == View.GONE) {
                    update.setVisibility(View.VISIBLE);
                    select.setVisibility(View.VISIBLE);
                    after_sel.setVisibility(View.GONE);
                    break;
                }
            case R.id.cancel_event:
                if (cancel.getVisibility() == View.VISIBLE) {
                    cancel.setVisibility(View.GONE);
                    break;
                } else if (cancel.getVisibility() == View.GONE) {
                    cancel.setVisibility(View.VISIBLE);
                    break;
                }

        }
    }

    public void sel_update(View v) {
        Spinner events_available = (Spinner) findViewById(R.id.events_available_sp);
        LinearLayout select = (LinearLayout) findViewById(R.id.select_to_update);
        LinearLayout after_sel = (LinearLayout) findViewById(R.id.parent_ll_update);
        TextView edit_date = (TextView) findViewById(R.id.et_date_modify);
        EditText edit_cap = (EditText) findViewById(R.id.et_cap_update);
        Spinner campus_et = (Spinner) findViewById(R.id.hosted_at_modify_sp);
        Spinner host_et = (Spinner) findViewById(R.id.hosted_by_modify_sp);

        String value = events_available.getSelectedItem().toString().trim();
        String[] parts = value.split(" - ");
        String event_id = parts[0].trim();
        String event_name = parts[1].trim();
        String savedData = dm.getDataOfEvents(event_id);
        String[] parts2 = savedData.split(" - ");
        String date_data = parts2[0].trim();
        String capacity_data = parts2[1].trim();
        switch (v.getId()) {
            case R.id.up_sel_btn:
                select.setVisibility(View.GONE);
                after_sel.setVisibility(View.VISIBLE);
                edit_date.setText(date_data);
                edit_cap.setText(capacity_data);
                break;
            case R.id.cancel_up_event:
                select.setVisibility(View.VISIBLE);
                after_sel.setVisibility(View.GONE);
                reset();
                break;
            case R.id.update_event_btn:
                String date_new = edit_date.getText().toString().trim();
                String cap_new = edit_cap.getText().toString().trim();
                String campus_new = campus_et.getSelectedItem().toString().trim();
                String host_new = host_et.getSelectedItem().toString().trim();
                if (dp.valid_date(date_data, date_new) == true && dp.isNumeric(cap_new)==true
                        && (dp.possiblecap(capacity_data,cap_new) <= dm.getAvailable(event_id))) {
                    dm.update2(event_id, date_new, campus_new, host_new, cap_new);
                    dm.update3(cap_new,event_id);
                    reset();
                    Toast.makeText(getBaseContext(),"Event Updated",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getBaseContext(), "Update failed, Please correct the date.", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    public void reset(){
        ((Spinner) findViewById(R.id.events_available_sp)).setSelection(0);
        LinearLayout select = (LinearLayout) findViewById(R.id.select_to_update);
        LinearLayout after_sel = (LinearLayout) findViewById(R.id.parent_ll_update);
        select.setVisibility(View.VISIBLE);
        after_sel.setVisibility(View.GONE);
        ((EditText) findViewById(R.id.et_date_modify)).setText("");
        ((EditText) findViewById(R.id.et_cap_update)).setText("");
        ((Spinner) findViewById(R.id.hosted_at_modify_sp)).setSelection(0);
        ((Spinner) findViewById(R.id.hosted_by_modify_sp)).setSelection(0);
    }

    public void reset2(){
        ((Spinner) findViewById(R.id.events_available_sp)).setSelection(0);
        LinearLayout select = (LinearLayout) findViewById(R.id.select_to_update);
        LinearLayout after_sel = (LinearLayout) findViewById(R.id.parent_ll_update);
        select.setVisibility(View.VISIBLE);
        after_sel.setVisibility(View.GONE);
        ((EditText) findViewById(R.id.et_date_modify)).setText("");
        ((EditText) findViewById(R.id.et_cap_update)).setText("");
        ((Spinner) findViewById(R.id.hosted_at_modify_sp)).setSelection(0);
        ((Spinner) findViewById(R.id.hosted_by_modify_sp)).setSelection(0);
    }

    public void back(View v){
        this.finish();
    }

}
