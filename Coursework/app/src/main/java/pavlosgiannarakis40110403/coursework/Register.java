package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.app.ListActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Register extends Activity implements View.OnClickListener{
    private DatabaseManipulator dm;
    private DataProtection dp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_register);

        Spinner options_reg_list = (Spinner) findViewById(R.id.options_reg_list);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
        R.array.sign_in_list, android.R.layout.simple_spinner_item);
        options_reg_list.setAdapter(adapter);
        this.dm = new DatabaseManipulator(this);
        this.dp = new DataProtection(this);

        View submit = findViewById(R.id.submit_btn);
        submit.setOnClickListener(this);
        View back = findViewById(R.id.back_btn);
        back.setOnClickListener(this);
    }

    public void onClick ( View v ) {
        switch (v. getId () ) {
            case R.id.back_btn:
                this.finish() ;
                break;
            case R.id.submit_btn:
                String name=((EditText)findViewById(R.id.et_name)).getText().toString().trim();
                String surname=((EditText)findViewById(R.id.et_surname)).getText().toString().trim();
                String id=((EditText)findViewById(R.id.et_id)).getText().toString().trim();
                String email=((EditText)findViewById(R.id.et_email)).getText().toString().trim();
                Spinner spinner = (Spinner)findViewById(R.id.options_reg_list);
                String role = spinner.getSelectedItem().toString().trim();
                String password = ((EditText)findViewById(R.id.et_pass)).getText().toString().trim();
                String password2 = ((EditText)findViewById(R.id.et_cnfg_pass)).getText().toString().trim();

                Log.i("test",""+dp.DataValidation(name, surname, id, email, role , password, password2));

               if(dp.DataValidation(name, surname, id, email, role , password, password2)==5)
                {
                this.dm.insert(name, surname,id, email, role , password);
                this.dm.update1(id);
                Toast.makeText(getBaseContext(), "Person added", Toast.LENGTH_LONG).show();
                reset_fields();
                Log.i("test","person added");
                    break;
                }
                else
                {
                    Toast.makeText(getBaseContext(),"Please add correct data.",Toast.LENGTH_SHORT).show();
                }
                break ;
        }
    }

    public void reset_fields()
    {
        ((EditText)findViewById(R.id.et_name)).setText("");
        ((EditText)findViewById(R.id.et_surname)).setText("");
        ((EditText)findViewById(R.id.et_id)).setText("");
        ((EditText)findViewById(R.id.et_email)).setText("");
        Spinner spinner = (Spinner)findViewById(R.id.options_reg_list);
        spinner.setSelection(0);
        ((EditText)findViewById(R.id.et_pass)).setText("");
        ((EditText)findViewById(R.id.et_cnfg_pass)).setText("");
    }
}
