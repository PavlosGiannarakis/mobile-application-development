package pavlosgiannarakis40110403.coursework;

import android.app.ListActivity;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;


public class Show_list extends ListActivity {
    DatabaseManipulator dm;
    List<String[]> list = new ArrayList<String[]>();
    List<String[]> data = null;
    String[] stg1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);

        dm = new DatabaseManipulator(this);

        Bundle extras = getIntent().getExtras();
        String value = extras.getString("Selected");
        String value2 = extras.getString("Selection");

        Log.i("test",""+value + "," + value2 );
        fill_list(value,value2);
    }

    public void fill_list(String value,String value2){
        if (value.equals("Event"))
        {
            getAllEvents();
        }
        else if (value2.equals("") && value.equals("All"))
        {
            getdata4();
        }
        else if (value2.equals("") && value.equals("Teacher"))
        {
            getdata3(value);
        }
        else if (value2.equals("") && value.equals("Student"))
        {
            getdata3(value);
        }
        else if (value2.equals("Registered") && value.equals("All"))
        {
            getdata2();
        }
        else if (value2.equals("Registered") && value.equals("Teacher"))
        {
            getdata(value, value2);
        }
        else if (value2.equals("Registered") && value.equals("Student"))
        {
            getdata(value, value2);
        }
    }

    public void getdata(String role, String reg_status) {
        data = dm.select(role, reg_status);
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name:data) {
            stg = name[0] + " - " + name[1] + " - " + name[2];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
    }

    public void getdata2() {
        data = dm.selectAllReg();
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name : data) {
            stg = name[0] + " - " + name[1] + " - " + name[2];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
    }

    public void getdata3(String role) {
        data = dm.select2(role);
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name : data) {
            stg = name[0] + " - " + name[1] + " - " + name[2];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
    }

    public void getdata4() {
        data = dm.selectAll2();
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name : data) {
            stg = name[0] + " - " + name[1] + " - " + name[2];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
    }

    public void getAllEvents() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        data = dm.getAllDataEvents();
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name : data) {
            stg = name[1] + " - " + name[2]+" - "+name[3]+ " - " + name[6]+ " - " + name[7]+ " - " + name[8];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
    }

    public void back(View v)
    {
        this.finish();
    }
}
