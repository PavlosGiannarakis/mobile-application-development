package pavlosgiannarakis40110403.coursework;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import java.util.ArrayList;
import java.util.List;


public class DatabaseManipulator {
    private static final String DATABASE_NAME = "coursework.db";
    private static int DATABASE_VERSION = 1;
    private static Context context;
    static SQLiteDatabase db;

    static final String TABLE_NAME = "register";
    static final String TABLE_NAME2 = "people";
    static final String TABLE_NAME3 = "events";
    static final String TABLE_NAME4 = "attendance";
    static final String TABLE_NAME5 = "cancellations";
    static final String TABLE_NAME6 = "Students_enrolled";

    private SQLiteStatement insertStmt;
    private SQLiteStatement insertStmt2;
    private SQLiteStatement insertStmt3;
    private SQLiteStatement insertStmt4;
    private SQLiteStatement insertStmt5;
    private SQLiteStatement insertStmt6;
    private SQLiteStatement updateStmt;
    private SQLiteStatement updateStmt2;
    private SQLiteStatement updateStmt3;
    private SQLiteStatement updateStmt4;
    private SQLiteStatement cancelStmt;

    private static final String INSERT = "insert into " + TABLE_NAME
            + " (name, surname, id, email, role, pass, reg_status) values (?,?,?,?,?,?,?)";

    private static final String INSERT2 = "insert into " + TABLE_NAME2
            + " (id, role, reg_status) values (?,?,?)";

    private static final String INSERT3 = "insert into " + TABLE_NAME3
            + " (event_id, event_name, date_on, hosted_at, hosted_by, created_on, created_by, status,"+
            " holds) values (?,?,?,?,?,?,?,?,?)";

    private static final String INSERT4 = "insert into " + TABLE_NAME4
            + " (event_id, capacity, available) values (?,?,?)";

    private static final String INSERT5 = "insert into " + TABLE_NAME5
            + " (event_id, event_name, reason_fc) values (?,?,?)";

    private static final String INSERT6 = "insert into " + TABLE_NAME6
            + " (id, event_id) values (?,?)";

    private static final String UPDATE = "update " + TABLE_NAME2
            + " set reg_status = ? where id = ?";

    private static final String UPDATE2 = "update " + TABLE_NAME3
            + " set date_on = ? ,hosted_at = ? , hosted_by = ? , holds = ? " +
            "where event_id = ?";

    private static final String UPDATE3 = "update " + TABLE_NAME4
            + " set available = ? where event_id = ? ";

    private static final String UPDATE4 = "update " + TABLE_NAME4
            + " set capacity = ? , available = ?  where event_id = ? ";

    private static final String CANCEL = "update " + TABLE_NAME3
            + " set status = 'Cancelled' where event_id = ?";

    public DatabaseManipulator(Context context) {
        DatabaseManipulator.context = context;
        OpenHelper openHelper = new OpenHelper(DatabaseManipulator.context);
        DatabaseManipulator.db = openHelper.getWritableDatabase();
        this.insertStmt = DatabaseManipulator.db.compileStatement(INSERT);
        this.insertStmt2 = DatabaseManipulator.db.compileStatement(INSERT2);
        this.insertStmt3 = DatabaseManipulator.db.compileStatement(INSERT3);
        this.insertStmt4 = DatabaseManipulator.db.compileStatement(INSERT4);
        this.insertStmt5 = DatabaseManipulator.db.compileStatement(INSERT5);
        this.insertStmt6 = DatabaseManipulator.db.compileStatement(INSERT6);
        this.updateStmt = DatabaseManipulator.db.compileStatement(UPDATE);
        this.updateStmt2 = DatabaseManipulator.db.compileStatement(UPDATE2);
        this.updateStmt3 = DatabaseManipulator.db.compileStatement(UPDATE3);
        this.updateStmt4 = DatabaseManipulator.db.compileStatement(UPDATE4);
        this.cancelStmt = DatabaseManipulator.db.compileStatement(CANCEL);
    }

    public long insert(String name, String surname, String id, String email, String role, String pass) {
        this.insertStmt.bindString(1, name);
        this.insertStmt.bindString(2, surname);
        this.insertStmt.bindString(3, id);
        this.insertStmt.bindString(4, email);
        this.insertStmt.bindString(5, role);
        this.insertStmt.bindString(6, pass);
        this.insertStmt.bindString(7, "Registered");
        return this.insertStmt.executeInsert();
    }

    public long insert2(String id, String role){
        this.insertStmt2.bindString(1,id);
        this.insertStmt2.bindString(2,role);
        this.insertStmt2.bindString(3, "Unregistered");
        return this.insertStmt2.executeInsert();
    }

    public long insert3(String event_id, String event_name, String date_on, String hosted_at,
                             String hosted_by, String created_on, String created_by,
                             String status, String holds) {
        this.insertStmt3.bindString(1, event_id);
        this.insertStmt3.bindString(2, event_name);
        this.insertStmt3.bindString(3, date_on);
        this.insertStmt3.bindString(4, hosted_at);
        this.insertStmt3.bindString(5, hosted_by);
        this.insertStmt3.bindString(6, created_on);
        this.insertStmt3.bindString(7, created_by);
        this.insertStmt3.bindString(8, status);
        this.insertStmt3.bindString(9, holds);
        return this.insertStmt3.executeInsert();
    }

    public long insert4(String event_id, String capacity, String available) {
        this.insertStmt4.bindString(1, event_id);
        this.insertStmt4.bindString(2, capacity);
        this.insertStmt4.bindString(3, available);
        return this.insertStmt4.executeInsert();
    }

    public long insert5(String event_id, String event_name, String reason) {
        this.insertStmt5.bindString(1, event_id);
        this.insertStmt5.bindString(2, event_name);
        this.insertStmt5.bindString(3, reason);
        return this.insertStmt5.executeInsert();
    }

    public long insert6(String id,String event_id)
    {
        this.insertStmt6.bindString(1, id);
        this.insertStmt6.bindString(2, event_id);
        return this.insertStmt6.executeInsert();
    }

    public long update1(String id){
        this.updateStmt.bindString(1,"Registered");
        this.updateStmt.bindString(2, id);
        return this.updateStmt.executeInsert();
    }

    public long update2(String id,String date,String place,String host,String cap)
    {
        this.updateStmt2.bindString(1,date);
        this.updateStmt2.bindString(2,place);
        this.updateStmt2.bindString(3,host);
        this.updateStmt2.bindString(4,cap);
        this.updateStmt2.bindString(5,id);
        return this.updateStmt2.executeInsert();
    }

    public long update3(String available,String id){
        this.updateStmt3.bindString(1, available);
        this.updateStmt3.bindString(2, id);
        return this.updateStmt3.executeInsert();
    }

    public long update4(String cap,String available,String id){
        this.updateStmt4.bindString(1,cap);
        this.updateStmt4.bindString(2,available);
        this.updateStmt4.bindString(3, id);
        return this.updateStmt4.executeInsert();
    }

    public long cancelEvent(String id)
    {
        this.cancelStmt.bindString(1,id);
        return this.cancelStmt.executeInsert();
    }


    public void deleteAll() {
        db.delete(TABLE_NAME, null, null);

    }

    public List<String[]> selectAll() {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME, new String[]{"name", "surname", "id", "email", "role", "pass","reg_status"}, null, null, null, null,null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b1 = new String[]{cursor.getString(0),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6)};
                list.add(b1);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public List<String[]> select(String role, String reg_status) {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME2, new String[]{"id", "role", "reg_status"}," role = ? AND reg_status = ?",
                new String[]{role,reg_status}, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b2 = new String[]{cursor.getString(0),
                        cursor.getString(1), cursor.getString(2)};
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public List<String[]> select2(String role) {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME2, new String[]{"id", "role", "reg_status"}," role = ? ",
                new String[]{role}, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b2 = new String[]{cursor.getString(0),
                        cursor.getString(1), cursor.getString(2)};
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public List<String[]> selectAll2() {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME2, new String[]{"id", "role", "reg_status"},null,
                null, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b2 = new String[]{cursor.getString(0),
                        cursor.getString(1), cursor.getString(2)};
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public List<String[]> selectAllReg() {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME2, new String[]{"id", "role", "reg_status"}," reg_status = ? ",
                new String[]{"Registered"}, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b2 = new String[]{cursor.getString(0),
                        cursor.getString(1), cursor.getString(2)};
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public int valid_id(String enter, String enter2) {
        Cursor cursor = db.query(TABLE_NAME2, new String[]{ "id" }," id = ? AND role = ? ",
                new String[] { enter,enter2 },null, null, null);
        int x=cursor.getCount();
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return x;
    }

    public void del_id(String enter) {
        db.delete(TABLE_NAME," id = ? ",new String[]{enter});
        db.delete(TABLE_NAME2," id = ? ",new String[]{enter});
    }

    public int valid_pass(String enter, String enter2, String enter3 ) {
        Cursor cursor = db.query(TABLE_NAME, new String[]{ "id", "pass", "role"},"id = ? AND pass = ? AND role = ? ",
                new String[] { enter, enter2, enter3 },null, null, null);
        int x=cursor.getCount();
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return x;
    }

    public String getName(String id) {
        Cursor cursor = db.query(TABLE_NAME, new String[]{ "name" },"id = ? ",
                new String[] { id },null, null, null);
        cursor.moveToFirst();
        String name = cursor.getString(0);
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return name;
    }

    public List<String> getTeachers() {
        List<String> list = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME2 + " WHERE role = ?";

        Cursor cursor = db.rawQuery(selectQuery,new String[]{"Teacher"});
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String b2 = cursor.getString(0);
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public List<String> getEvents() {
        List<String> list = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME3 + " WHERE status = ? ";

        Cursor cursor = db.rawQuery(selectQuery, new String[]{"Scheduled"});
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String b2 = cursor.getString(0)+" - "+cursor.getString(1);
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    public int getAvailable(String id)
    {
        List<String> list = new ArrayList<String>();
        String selectQuery = "SELECT available FROM " + TABLE_NAME4 + " WHERE event_id = ? ";

        Cursor cursor = db.rawQuery(selectQuery, new String[]{id});
        cursor.moveToFirst();
        String x = cursor.getString(0);
        int y = Integer.parseInt(x);
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return y;
    }

    public String getDataOfEvents(String id) {
        String b2 = "";
        Cursor cursor = db.query(TABLE_NAME3, new String[]{"date_on", "holds"}," event_id = ? ",
                new String[]{ id }, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                b2 = cursor.getString(0)+" - "+cursor.getString(1);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return b2;
    }

    public List<String[]> getAllDataEvents() {
        List<String[]> list = new ArrayList<String[]>();
        Cursor cursor = db.query(TABLE_NAME3, new String[]{"event_id", "event_name", "date_on",
                        "hosted_at", "hosted_by", "created_on", "created_by", "status", "holds"},
                null, null, null, null, null);
        int x = 0;
        if (cursor.moveToFirst()) {
            do {
                String[] b2 = new String[]{cursor.getString(0),cursor.getString(1),
                        cursor.getString(2),cursor.getString(3),cursor.getString(4),
                        cursor.getString(5),cursor.getString(6),cursor.getString(7),
                        cursor.getString(8)};
                list.add(b2);
                x++;
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return list;
    }

    private static class OpenHelper extends SQLiteOpenHelper {
            OpenHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

            public void onCreate(SQLiteDatabase db) {
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME
                        + " (name TEXT , surname TEXT, id TEXT , email TEXT , role TEXT, pass TEXT, reg_status TEXT)");
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME2
                        + " (id TEXT, role TEXT, reg_status TEXT)");
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME3
                        + " (event_id TEXT, event_name TEXT, date_on TEXT, hosted_at TEXT, hosted_by TEXT,"
                        + " created_on TEXT, created_by TEXT, status TEXT, holds TEXT)");
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME4
                        + " (event_id TEXT, capacity TEXT, available TEXT)");
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME5
                        + " (event_id TEXT, event_name TEXT, reason_fc TEXT)");
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME6
                        + " (id TEXT, event_id TEXT)");
            }

            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                DATABASE_VERSION = newVersion;
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME2);
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME3);
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME4);
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME5);
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME5);
                onCreate(db);
            }

        }

}