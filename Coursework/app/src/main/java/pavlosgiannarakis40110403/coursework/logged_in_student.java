package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;


public class logged_in_student extends Activity {
    private DatabaseManipulator dm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logged_in_student);
        dm= new DatabaseManipulator(this);

        Bundle extras = getIntent().getExtras();
        String value = extras.getString("User");
        Log.i("test", value);

        final TextView textViewToChange = (TextView) findViewById(R.id.user_wlc);
        textViewToChange.setText("Welcome "+dm.getName(value));
    }

    public void onTvClick2(View v) {
        Log.i("So it ", "works");
        Intent show_students = new Intent(logged_in_student.this,Show_list.class);
        Intent enroll_yourself = new Intent(logged_in_student.this,Enroll__students_on_events.class);
        LinearLayout cancel = (LinearLayout) findViewById(R.id.cancel_event_ll);
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("User");
        switch (v.getId()) {
            case R.id.show_enrolled:
                show_students.putExtra("Selected", "Student");
                show_students.putExtra("Selection", "");
                startActivity(show_students);
                break;
            case R.id.show_event:
                show_students.putExtra("Selected", "Event");
                show_students.putExtra("Selection", "");
                startActivity(show_students);
                break;
            case R.id.enroll_on_event:
                enroll_yourself.putExtra("Use", value);
                startActivity(enroll_yourself);
                break;
            case R.id.cancel_event:
                if (cancel.getVisibility() == View.VISIBLE) {
                    cancel.setVisibility(View.GONE);
                    break;
                } else if (cancel.getVisibility() == View.GONE) {
                    cancel.setVisibility(View.VISIBLE);
                    break;
                }

        }
    }

}
