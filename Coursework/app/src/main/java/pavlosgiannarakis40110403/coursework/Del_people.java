package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;


public class Del_people extends Activity {

    private DatabaseManipulator del_someone;
    private DataProtection dp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del_people);

        del_someone = new DatabaseManipulator(this);
        dp = new DataProtection(this);
    }

    public void onClick_del(View v)
    {
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("role");
        String id_del = ((EditText)findViewById(R.id.et_id_del)).getText().toString().trim();

        switch (v.getId()) {
            case R.id.b_btn_del:
                this.finish();
                break;
            case R.id.s_btn_del:
                switch (value) {
                    case "Teacher":
                        if (del_someone.valid_id(id_del, "Teacher") == 1)
                        {
                            del_someone.del_id(id_del);
                            Log.i("test", "deleted teacher");
                            Toast.makeText(getBaseContext(), "Teacher Deleted", Toast.LENGTH_LONG).show();
                            ((EditText)findViewById(R.id.et_id_del)).setText("");
                        }
                        else
                        {
                            Toast.makeText(getBaseContext(), "Please use the correct format", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case "Student":
                        if (del_someone.valid_id(id_del, "Student") == 1)
                        {
                            del_someone.del_id(id_del);
                            Log.i("test","deleted student");
                            Toast.makeText(getBaseContext(), "Student deleted", Toast.LENGTH_LONG).show();
                            ((EditText)findViewById(R.id.et_id_del)).setText("");
                        }
                        else
                        {
                            Toast.makeText(getBaseContext(), "Please use the correct format", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
        }

    }
}
