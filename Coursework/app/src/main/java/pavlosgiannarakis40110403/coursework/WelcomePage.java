package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.util.Log ;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


public class WelcomePage extends Activity{
    static final int DIALOG_ID = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_welcome_page);

        final Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            int counter = 0;
            public void onClick(View v) {

                Log.i("test"," Continue pressed ");

                Intent activity_sign_in = new Intent (WelcomePage.this, Sign_in.class);
                Intent activity_check_data = new Intent (WelcomePage.this, CheckData.class);

                Spinner options_list = (Spinner) findViewById(R.id.options_list);
                int text = options_list.getSelectedItemPosition();

                if (text == 0){
                    Log.i("test"," Wrong selection");
                    Toast.makeText(getBaseContext(), "Please select a value ", Toast.LENGTH_SHORT).show();
                    counter++;
                    if (counter >= 3)
                    {
                        Toast.makeText(getBaseContext(), "Are you looking to register?", Toast.LENGTH_LONG).show();
                    }
                }
                else if (text == 1){
                    Log.i("test"," Teacher");
                    activity_sign_in.putExtra("Category","Teacher");
                    startActivity(activity_sign_in);
                }
                else if (text == 2){
                    Log.i("test"," Student");
                    activity_sign_in.putExtra("Category","Student");
                    startActivity(activity_sign_in);
                }
                else if (text == 3){
                    Log.i("test"," Admin");
                    activity_sign_in.putExtra("Category","Admin");
                    startActivity(activity_sign_in);
                }
                else if (text == 4){
                    Log.i("test","Just checking");
                    startActivity(activity_check_data);
                }

            }
        });

        final Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("test"," Register pressed ");
                Intent activity_register = new Intent (WelcomePage.this, Register.
                        class);
                startActivity(activity_register);
            }
        });

        Spinner options_list = (Spinner) findViewById(R.id.options_list);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
        R.array.sign_in_list, android.R.layout.simple_spinner_item);
        options_list.setAdapter(adapter);
    }
}
