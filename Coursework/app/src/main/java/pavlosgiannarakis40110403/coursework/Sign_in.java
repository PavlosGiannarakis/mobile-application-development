package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;


public class Sign_in extends Activity implements View.OnClickListener {
    private DatabaseManipulator readDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_sign_in);

        readDb = new DatabaseManipulator(this);
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("Category");
        Log.i("test", value);

        final TextView textViewToChange = (TextView) findViewById(R.id.del);
        textViewToChange.setText(value);

        View submit = findViewById(R.id.sgn_btn);
        submit.setOnClickListener(this);
        View back = findViewById(R.id.back_sgn_btn);
        back.setOnClickListener(this);
    }
    public void onClick ( View v ) {
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("Category");

        switch (v. getId () ) {
            case R.id.back_sgn_btn:
                this.finish();
                break;
            case R.id.sgn_btn:
                String id=((EditText)findViewById(R.id.et_sgn_id)).getText().toString().trim();
                String pass=((EditText)findViewById(R.id.et_sgn_pass)).getText().toString().trim();
                Log.i("test", ""+readDb.valid_pass(id,pass,value));
                Intent activity_login = new Intent(Sign_in.this, logged_in_admin.class);
                Intent activity_login2 = new Intent(Sign_in.this, logged_in_teacher.class);
                Intent activity_login3 = new Intent(Sign_in.this, logged_in_student.class);
                if (readDb.valid_pass(id, pass, value) == 1)
                {
                    if (value.equals("Admin")) {
                        Log.i("test", "logging in as admin");
                        activity_login.putExtra("User", id);
                        startActivity(activity_login);
                        this.finish();
                    }
                    else if(value.equals("Teacher")){
                        Log.i("test", "logging in as a teacher");
                        activity_login2.putExtra("User", id);
                        startActivity(activity_login2);
                        this.finish();
                    }
                    else if(value.equals("Student")){
                        Log.i("test", "logging in as a student");
                        activity_login3.putExtra("User", id);
                        startActivity(activity_login3);
                        this.finish();
                    }
                }
                else
                {
                    this.finish();
                }
                break ;
        }
    }
}
