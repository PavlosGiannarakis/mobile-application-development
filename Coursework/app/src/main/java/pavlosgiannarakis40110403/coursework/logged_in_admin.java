package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class logged_in_admin extends Activity {
    private DatabaseManipulator dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_logged_in_admin);
        dm = new DatabaseManipulator(this);

        Bundle extras = getIntent().getExtras();
        String value = extras.getString("User");
        Log.i("test", value);

        final TextView textViewToChange = (TextView) findViewById(R.id.user_wlc);
        textViewToChange.setText("Welcome "+dm.getName(value));

    }

    public void onClickAdd(View v){
        int x = 0;
        boolean student = ((CheckBox) findViewById(R.id.add_stu)).isChecked();
        boolean teacher = ((CheckBox) findViewById(R.id.add_tc)).isChecked();
        Intent add = new Intent(logged_in_admin.this,Add_People.class);
        Log.i("test",""+teacher);
        Log.i("test",""+student);
        if ((teacher == true && student == true) || (teacher == false && student == false))
        {
            x=0;
        }
        else if (teacher == true && student == false)
        {
            x=1;
        }
        else if (teacher == false && student == true)
        {
            x=2;
        }
        switch (x){
            case 0:
                Toast.makeText(getBaseContext(), "Please select one value", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(getBaseContext(), "Teacher", Toast.LENGTH_LONG).show();
                add.putExtra("role","Teacher");
                startActivity(add);
                break;
            case 2:
                Toast.makeText(getBaseContext(), "Student", Toast.LENGTH_LONG).show();
                add.putExtra("role","Student");
                startActivity(add);
                break;
        }

    }

    public void onClickDel(View v){
        int x = 0;
        boolean student = ((CheckBox) findViewById(R.id.chk_del_stu)).isChecked();
        boolean teacher = ((CheckBox) findViewById(R.id.chk_del_tc)).isChecked();
        Intent del = new Intent(logged_in_admin.this,Del_people.class);
        Log.i("test",""+teacher);
        Log.i("test",""+student);
        if ((teacher == true && student == true) || (teacher == false && student == false))
        {
            x=0;
        }
        else if (teacher == true && student == false)
        {
            x=1;
        }
        else if (teacher == false && student == true)
        {
            x=2;
        }
        switch (x){
            case 0:
                Toast.makeText(getBaseContext(), "Please select one value", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(getBaseContext(), "Teacher", Toast.LENGTH_LONG).show();
                del.putExtra("role","Teacher");
                startActivity(del);
                break;
            case 2:
                Toast.makeText(getBaseContext(), "Student", Toast.LENGTH_LONG).show();
                del.putExtra("role","Student");
                startActivity(del);
                break;
        }

    }

    public void onClickShow(View v){
        String x = "";
        String y = "";
        boolean student = ((CheckBox) findViewById(R.id.stu_chk)).isChecked();
        boolean teacher = ((CheckBox) findViewById(R.id.tch_chk)).isChecked();
        boolean student1 = ((CheckBox) findViewById(R.id.stu_chk_reg)).isChecked();
        boolean teacher1 = ((CheckBox) findViewById(R.id.tch_chk_reg)).isChecked();
        Intent show_un = new Intent(logged_in_admin.this, Show_list.class);

        Log.i("test",""+teacher);
        Log.i("test",""+student);
        if (teacher == false && student == false)
        {
            x="nothing";
        }
        else if (teacher == true && student == false)
        {
            x="Teacher";
        }
        else if (teacher == false && student == true)
        {
            x="Student";
        }
        else if(teacher == true && student == true)
        {
            x="All";
        }



        if (teacher1 == false && student1 == false)
        {
            y="nothing";
        }
        else if (teacher1 == true && student1 == false)
        {
            y="Teacher";
        }
        else if (teacher1 == false && student1 == true)
        {
            y="Student";
        }
        else if(teacher1 == true && student1 == true)
        {
            y="All";
        }

        switch (v.getId()) {
            case R.id.btn_list:
                switch (x) {
                    case "nothing":
                        Toast.makeText(getBaseContext(), "Please select one value", Toast.LENGTH_LONG).show();
                        break;
                    case "Teacher":
                        Log.i("test", "teacher");
                        show_un.putExtra("Selected", x);
                        show_un.putExtra("Selection", "");
                        startActivity(show_un);
                        break;
                    case "Student":
                        Log.i("test", "student");
                        show_un.putExtra("Selected", x);
                        show_un.putExtra("Selection", "");
                        startActivity(show_un);
                        break;
                    case "All":
                        Log.i("test", "Both");
                        show_un.putExtra("Selected", x);
                        show_un.putExtra("Selection", "");
                        startActivity(show_un);
                        break;
                }
            case R.id.btn_list_reg:
                switch (y) {
                    case "nothing":
                        Toast.makeText(getBaseContext(), "Please select one value", Toast.LENGTH_LONG).show();
                        break;
                    case "Teacher":
                        Log.i("test", "teacher");
                        show_un.putExtra("Selected", y);
                        show_un.putExtra("Selection", "Registered");
                        startActivity(show_un);
                        break;
                    case "Student":
                        Log.i("test", "student");
                        show_un.putExtra("Selected", y);
                        show_un.putExtra("Selection", "Registered");
                        startActivity(show_un);
                        break;
                    case "All":
                        Log.i("test", "Both");
                        show_un.putExtra("Selected", y);
                        show_un.putExtra("Selection", "Registered");
                        startActivity(show_un);
                        break;
                }

        }
    }

    public void onTvClick(View v){
        Log.i("So it ","works");
        LinearLayout show_people =(LinearLayout)findViewById(R.id.show_p_ll);
        LinearLayout show_people_reg =(LinearLayout)findViewById(R.id.show_p_reg_ll);
        LinearLayout add_people =(LinearLayout)findViewById(R.id.add_p);
        LinearLayout del_people =(LinearLayout)findViewById(R.id.del_p);

        switch(v.getId()){
            case R.id.show_p:
                if (show_people.getVisibility() == View.VISIBLE) {
                    show_people.setVisibility(View.GONE);
                    break;
                }
                else if (show_people.getVisibility() == View.GONE)
                {
                    show_people.setVisibility(View.VISIBLE);
                    break;
                }
            case R.id.show_p_reg:
                if (show_people_reg.getVisibility() == View.VISIBLE) {
                    show_people_reg.setVisibility(View.GONE);
                    break;
                }
                else if (show_people_reg.getVisibility() == View.GONE)
                {
                    show_people_reg.setVisibility(View.VISIBLE);
                    break;
                }
            case R.id.add_someone:
                if (add_people.getVisibility() == View.VISIBLE) {
                    add_people.setVisibility(View.GONE);
                    break;
                }
                else if (add_people.getVisibility() == View.GONE)
                {
                    add_people.setVisibility(View.VISIBLE);
                    break;
                }
            case R.id.del_someone:
                if (del_people.getVisibility() == View.VISIBLE) {
                    del_people.setVisibility(View.GONE);
                    break;
                }
                else if (del_people.getVisibility() == View.GONE)
                {
                    del_people.setVisibility(View.VISIBLE);
                    break;
                }

        }
    }
}
