package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class Add_People extends Activity {
    private DatabaseManipulator add_someone;
    private DataProtection dp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__people);

        add_someone = new DatabaseManipulator(this);
        dp = new DataProtection(this);
    }

    public void onClick(View v)
    {
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("role");
        String id_add = ((EditText)findViewById(R.id.et_id_add)).getText().toString().trim();

        switch (v.getId()) {
            case R.id.b_btn_add:
                this.finish();
                break;
            case R.id.s_btn_add:
                switch (value) {
                    case "Teacher":
                        if (dp.AdminDataValidation(id_add, "Teacher")==true)
                        {
                            add_someone.insert2(id_add, "Teacher");
                            Log.i("test","added teacher");
                            Toast.makeText(getBaseContext(), "Teacher added", Toast.LENGTH_LONG).show();
                            ((EditText)findViewById(R.id.et_id_add)).setText("");
                        }
                        else
                        {
                            Toast.makeText(getBaseContext(), "Please use the correct format", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case "Student":
                        if (dp.AdminDataValidation(id_add, "Student")==true)
                        {
                            add_someone.insert2(id_add,"Student");
                            Log.i("test","added student");
                            Toast.makeText(getBaseContext(), "Student added", Toast.LENGTH_LONG).show();
                            ((EditText)findViewById(R.id.et_id_add)).setText("");
                        }
                        else
                        {
                            Toast.makeText(getBaseContext(), "Please use the correct format", Toast.LENGTH_LONG).show();
                        }
                        break;
            }
        }

    }

}
