package pavlosgiannarakis40110403.coursework;

import android.app.Activity;
import android.app.ListActivity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Enroll__students_on_events extends ListActivity {
    TextView selection;
    DatabaseManipulator dm;
    List<String[]> list = new ArrayList<String[]>();
    List<String[]> data = null;
    String[] stg1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_enroll__students_on_events);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        dm = new DatabaseManipulator(this);

        data = dm.getAllDataEvents();
        stg1 = new String[data.size()];
        int x = 0;
        String stg;

        for (String[] name : data) {
            stg = name[0]+" - "+ name[1] + " - " + name[2]+" - "+name[3]+ " - " + name[6]+ " - " + name[7]+ " - " + name[8];
            stg1[x] = stg;
            x++;
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                stg1);
        this.setListAdapter(adapter2);
        selection = (TextView)findViewById(R.id.check_selection);
    }

    public void back2(View v)
    {
        this.finish();
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        selection.setText(stg1[position]);
    }

    public void enrollClick(View v)
    {
        String data = selection.getText().toString().trim();
        String[] data2 = data.split(" - ");
        String id2 = data2[0];
        String available = data2[6];
        Log.i("test",""+id2+", "+available);
        Bundle extras = getIntent().getExtras();
        String value = extras.getString("Use");
        if (dm.getAvailable(id2)>= 1 ) {
            int new_available= dm.getAvailable(id2)-1;
            String new_s_a = Integer.toString(new_available);
            dm.update3(new_s_a,id2);
            dm.insert6(id2,value);
        }
        else {
            Toast.makeText(getBaseContext(),"Please correct your mistakes",Toast.LENGTH_SHORT).show();
        }
    }
}
