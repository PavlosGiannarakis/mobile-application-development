package pavlosgiannarakis40110403.coursework;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class ReadDB {
    private static final String DATABASE_NAME = "coursework.db";
    private static int DATABASE_VERSION = 1;
    static final String TABLE_NAME = "register";
    private static Context context;
    static SQLiteDatabase db;


    public ReadDB(Context context) {
        ReadDB.context = context;
        OpenHelper openHelper = new OpenHelper(ReadDB.context);
        ReadDB.db = openHelper.getReadableDatabase();
    }

    public int valid_pass(String enter, String enter2) {
        Cursor cursor = db.query(TABLE_NAME, new String[]{ "id", "pass"},"id = ? AND pass = ? ",new String[] { enter, enter2 },null, null, null);
        int x=cursor.getCount();
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        cursor.close();
        return x;
    }

    private static class OpenHelper extends SQLiteOpenHelper {
            OpenHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

           public void onCreate(SQLiteDatabase db) {
                db.execSQL(" CREATE TABLE "
                        + TABLE_NAME
                        + " (name TEXT , surname TEXT, id TEXT , email TEXT , role TEXT, pass TEXT )");
            }

            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                DATABASE_VERSION = newVersion;
                db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
                onCreate(db);
            }

        }

}